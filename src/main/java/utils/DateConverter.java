package utils;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Converter for date and time in format of string to LocalDate and LocalTime
 */
public class DateConverter {
    /**
     * Convert given param to a localDate
     * @param date as string
     * @return result in form of local date
     */
    public static LocalDate convertStringToLocalDate(String date) {
        String[] array = date.split("-");
        LocalDate result = LocalDate.of(Integer.parseInt(array[0]), Integer.parseInt(array[1]),
                Integer.parseInt(array[2]));
        return result;
    }

    /**
     * Convert given param to a localTime
     * @param time as string
     * @return result in form of local time
     */
    public static LocalTime convertStringToLocalTime(String time) {
        String[] array = time.split(":");
        LocalTime result = LocalTime.of(Integer.parseInt(array[0]), Integer.parseInt(array[1]));
        return result;
    }
}
