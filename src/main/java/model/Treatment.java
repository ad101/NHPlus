package model;

import utils.DateConverter;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Treatments are carried out from caregiver to patient.
 */
public class Treatment {
    private long tid;
    private long pid;
    private long cid;
    private LocalDate date;
    private LocalTime begin;
    private LocalTime end;
    private String description;
    private String remarks;

    /**
     * constructs a treatment from the given params.
     * @param pid
     * @param cid
     * @param date
     * @param begin
     * @param end
     * @param description
     * @param remarks
     */
    public Treatment(long pid, long cid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks) {
        this.pid = pid;
        this.cid = cid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
    }

    /**
     * constructs a treatment from the given params.
     * @param tid
     * @param pid
     * @param cid
     * @param date
     * @param begin
     * @param end
     * @param description
     * @param remarks
     */
    public Treatment(long tid, long pid, long cid, LocalDate date, LocalTime begin,
                     LocalTime end, String description, String remarks) {
        this.tid = tid;
        this.pid = pid;
        this.cid = cid;
        this.date = date;
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.remarks = remarks;
    }

    /**
     *
     * @return treatment id
     */
    public long getTid() {
        return tid;
    }

    /**
     *
     * @return patient id
     */
    public long getPid() {
        return this.pid;
    }

    /**
     *
     * @return caregiver id
     */
    public long getCid() {
        return this.cid;
    }

    /**
     *
     * @return date of treatment as a string
     */
    public String getDate() {
        return date.toString();
    }

    /**
     *
     * @return time of treatment beginning as a string
     */
    public String getBegin() {
        return begin.toString();
    }

    /**
     *
     * @return time of treatment ending as a string
     */
    public String getEnd() {
        return end.toString();
    }

    /**
     * convert given param to a localDate and store as new <code>date</code>
     * @param s_date as string
     */
    public void setDate(String s_date) {
        LocalDate date = DateConverter.convertStringToLocalDate(s_date);
        this.date = date;
    }

    /**
     * convert given param to a localDate and store as new <code>time</code>
     * @param begin as string
     */
    public void setBegin(String begin) {
        LocalTime time = DateConverter.convertStringToLocalTime(begin);
        this.begin = time;
    }

    /**
     * convert given param to a localDate and store as new <code>time</code>
     * @param end as string
     */
    public void setEnd(String end) {
        LocalTime time = DateConverter.convertStringToLocalTime(end);
        this.end = time;
    }

    /**
     *
     * @return description as a string
     */
    public String getDescription() {
        return description;
    }

    /**
     * store given param as new <code>description</code>
     * @param description as string
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return remarks as a string
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * store given param as new <code>remarks</code>
     * @param remarks as string
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     *
     * @return string-representation of the treatment
     */
    public String toString() {
        return "\nBehandlung" + "\nTID: " + this.tid +
                "\nPID: " + this.pid +
                "\nCID: " + this.cid +
                "\nDate: " + this.date +
                "\nBegin: " + this.begin +
                "\nEnd: " + this.end +
                "\nDescription: " + this.description +
                "\nRemarks: " + this.remarks + "\n";
    }
}