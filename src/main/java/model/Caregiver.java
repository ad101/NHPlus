package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Caregiver work in a NURSING home and treat patients.
 */
public class Caregiver extends Person {
    private long cid;
    private String phoneNumber;
    private String status;
    private List<Treatment> allTreatments = new ArrayList<Treatment>();

    /**
     * Constructs a caregiver from the given params.
     * @param firstName
     * @param surname
     * @param phoneNumber
     * @param status
     */
    public Caregiver(String firstName, String surname, String phoneNumber, String status) {
        super(firstName, surname);
        this.phoneNumber = phoneNumber;
        this.status = status;
    }

    /**
     * Constructs a caregiver from the given params.
     * @param cid
     * @param firstName
     * @param surname
     * @param phoneNumber
     * @param status
     */
    public Caregiver(long cid, String firstName, String surname, String phoneNumber, String status) {
        super(firstName, surname);
        this.cid = cid;
        this.phoneNumber = phoneNumber;
        this.status = status;
    }

    /**
     *
     * @return caregiver id
     */
    public long getCid() {
        return cid;
    }

    public String getCidString()
        {
            String cidString = String.valueOf(cid);
            return cidString;
        }

    /**
     *
     * @return phoneNumber as string
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     *
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status change
     */
    public void setStatus(String status) {
        this.status = status;
    }


    /**
     * Adds a treatment to the treatment-list, if it does not already contain it.
     * @param m Treatment
     * @return true if the treatment was not already part of the list; otherwise false
     */
    public boolean add(Treatment m) {
        if (!this.allTreatments.contains(m)) {
            this.allTreatments.add(m);
            return true;
        }
        return false;
    }

    /**
     *
     * @return string-representation of the caregiver
     */
    public String toString() {
        return "Caregiver" + "\nCID: " + this.cid +
                "\nFirstname: " + this.getFirstName() +
                "\nSurname: " + this.getSurname() +
                "\nPhone: " + this.phoneNumber +
                "\nStatus: " + this.status +
                "\n";
    }
}