package model;

/**
 * Person is abstract class for patients and caregiver
 */
public abstract class Person {
    private String firstName;
    private String surname;

    /**
     * constructs a person from the given params.
     * @param firstName
     * @param surname
     */
    public Person(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }

    /**
     *
     * @return firstName as a string
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set a new <code>firstName</code>
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return surname as a string
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Set a new <code>surname</code>
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }
}
