package controller;

import datastorage.CaregiverDAO;
import datastorage.DAOFactory;
import datastorage.TreatmentDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import model.Caregiver;
import java.sql.SQLException;
import java.util.List;


/**
 * The <code>AllCaregiverController</code> contains the entire logic of the caregiver view.
 * It determines which data is displayed and how to react to events.
 */
public class AllCaregiverController {
    @FXML
    private TableView<Caregiver> tableView;
    @FXML
    private TableColumn<Caregiver, Integer> colID;
    @FXML
    private TableColumn<Caregiver, String> colFirstName;
    @FXML
    private TableColumn<Caregiver, String> colSurname;
    @FXML
    private TableColumn<Caregiver, String> colPhoneNumber;
    @FXML
    private TableColumn<Caregiver, String> colStatus;

    @FXML
    Button btnDelete;
    @FXML
    Button btnAdd;
    @FXML
    TextField txtSurname;
    @FXML
    TextField txtFirstname;
    @FXML
    TextField txtPhoneNumber;
    @FXML
    TextField txtStatus;
    @FXML
    private ComboBox<String> comboBox;

    private ObservableList<Caregiver> tableviewContent = FXCollections.observableArrayList();
    private ObservableList<String> myComboBoxData = FXCollections.observableArrayList();
    private CaregiverDAO dao;

    /**
     * Initializes the corresponding fields. Is called as soon as the corresponding FXML file is to be displayed.
     */
    public void initialize() {
        readAllAndShowInTableView();
        comboBox.setItems(myComboBoxData);
        comboBox.getSelectionModel().select(0);

        this.colID.setCellValueFactory(new PropertyValueFactory<Caregiver, Integer>("cid"));

        //CellValuefactory zum Anzeigen der Daten in der TableView
        this.colFirstName.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("firstName"));
        //CellFactory zum Schreiben innerhalb der Tabelle
        this.colFirstName.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colSurname.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("surname"));
        this.colSurname.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colPhoneNumber.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("phoneNumber"));
        this.colPhoneNumber.setCellFactory(TextFieldTableCell.forTableColumn());

        this.colStatus.setCellValueFactory(new PropertyValueFactory<Caregiver, String>("status"));
        this.colStatus.setCellFactory(TextFieldTableCell.forTableColumn());

        //Anzeigen der Daten
        this.tableView.setItems(this.tableviewContent);
        createComboBoxData();
    }

    /**
     * Handles new firstname value
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditFirstname(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setFirstName(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new surname value
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditSurname(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setSurname(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new phone number value
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditPhoneNumber(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setPhoneNumber(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Handles new status value
     * @param event event including the value that a user entered into the cell
     */
    @FXML
    public void handleOnEditStatus(TableColumn.CellEditEvent<Caregiver, String> event){
        event.getRowValue().setStatus(event.getNewValue());
        doUpdate(event);
    }

    /**
     * Updates a caregiver by calling the update-Method in the {@link CaregiverDAO}
     * @param t row to be updated by the user (includes the caregiver)
     */
    private void doUpdate(TableColumn.CellEditEvent<Caregiver, String> t) {
        try {
            dao.update(t.getRowValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls readAll in {@link CaregiverDAO} and shows caregiver in the table
     */
    private void readAllAndShowInTableView() {
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createCaregiverDAO();
        List<Caregiver> allCaregivers;
        try {
            allCaregivers = dao.readAll();
            for (Caregiver c : allCaregivers) {
                this.tableviewContent.add(c);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create ComboBox-list of caregivers statuses.
     */
    private void createComboBoxData(){
        this.myComboBoxData.add("alle");
        this.myComboBoxData.add("activ");
        this.myComboBoxData.add("gesperrt");
    }

    /**
     * Handles a ComboBox-filter-event. Show all/active/blocked caregivers.
     */
    @FXML
    public void handleComboBox(){
        String p = this.comboBox.getSelectionModel().getSelectedItem();
        this.tableviewContent.clear();
        this.dao = DAOFactory.getDAOFactory().createCaregiverDAO();
        List<Caregiver> allCaregivers;

        if(p.equals("alle")){
            try {
                allCaregivers= this.dao.readAll();
                for (Caregiver caregiver : allCaregivers) {
                    this.tableviewContent.add(caregiver);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(p.equals("activ")){
            try {
                allCaregivers= this.dao.readAll();
                for (Caregiver caregiver : allCaregivers) {
                    if (caregiver.getStatus().equals("activ")){
                        this.tableviewContent.add(caregiver);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(p.equals("gesperrt")){
            try {
                allCaregivers= this.dao.readAll();
                for (Caregiver caregiver : allCaregivers) {
                    if (caregiver.getStatus().equals("gesperrt")){
                        this.tableviewContent.add(caregiver);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Handles a delete-click-event. Calls the delete methods in the {@link CaregiverDAO} and {@link TreatmentDAO}
     */
    @FXML
    public void handleDeleteRow() {
        TreatmentDAO tDao = DAOFactory.getDAOFactory().createTreatmentDAO();
        Caregiver selectedItem = this.tableView.getSelectionModel().getSelectedItem();

        try {
            if (tDao.areAllTreatmentsOldByCid(selectedItem.getCid())){
                tDao.deleteByPid(selectedItem.getCid());
                dao.deleteById(selectedItem.getCid());
                this.tableView.getItems().remove(selectedItem);
            }
            else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText("Die Daten können nicht gelöscht werden");
                alert.setContentText("Die Daten von Pflegeperson können nicht gelöscht werden, \n" +
                        "weil ihre letzte Behandlung weniger als 10 Jahre zurückliegt. \n" +
                        "Wenn die Pfleger-Daten nicht mehr benötigt werden, \n" +
                        "die Frist aber noch nicht abgelaufen ist, \n" +
                        "können Sie die Pfleger-Daten sperren.\n");
                alert.showAndWait();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles a add-click-event. Creates a caregiver and calls the create method in the {@link CaregiverDAO}
     */
    @FXML
    public void handleAdd() {
        String surname = this.txtSurname.getText();
        String firstname = this.txtFirstname.getText();
        String phone = this.txtPhoneNumber.getText();
        String status = this.txtStatus.getText();
        try {
            Caregiver c = new Caregiver(firstname, surname, phone, status);
            dao.create(c);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        readAllAndShowInTableView();
        clearTextfields();
    }

    /**
     * Removes content from all textfields
     */
    private void clearTextfields() {
        this.txtFirstname.clear();
        this.txtSurname.clear();
        this.txtPhoneNumber.clear();
        this.txtStatus.clear();
    }
}