package datastorage;

/**
 * Implements a pattern to generate only one instance of DAO
 */
public class DAOFactory {

    private static DAOFactory instance;

    private DAOFactory() {

    }

    /**
     * Generates an instance if that is not exists yet
     * @return instance
     */
    public static DAOFactory getDAOFactory() {
        if (instance == null) {
            instance = new DAOFactory();
        }
        return instance;
    }

    public TreatmentDAO createTreatmentDAO() {
        return new TreatmentDAO(ConnectionBuilder.getConnection());
    }

    public PatientDAO createPatientDAO() {
        return new PatientDAO(ConnectionBuilder.getConnection());
    }

    public CaregiverDAO createCaregiverDAO() {
        return new CaregiverDAO(ConnectionBuilder.getConnection());
    }

}